import os
import sys
import getopt
import getpass
import json
import requests
import copy
import time
from bs4 import BeautifulSoup as bs
from docx import Document
import zipfile
from docx.shared import Inches

CYBER_ROOT_URL = 'https://ilearn.cyber.org.il'
STUDENT_EXERCISE_URL = CYBER_ROOT_URL + \
    '/teacher_freeform_assignment/to_grade/'
EXERCISE_FILE_URL = CYBER_ROOT_URL + '/teacher_freeform_assignment/grade/'
TO_GRADE_EXERCISE_URL = CYBER_ROOT_URL + '/teacher_assignments/to_grade/'
COURSE_STUDENTS_URL = CYBER_ROOT_URL + '/teacher_students/list/'
VERSION = "4.3"


def download_submission_file(session, exercise, student, out_dir):
    '''
    Downloads a student submission file for a given exercise.

    Stores file in current directory, with name ll_xx_NNN.docx, where
        ll  - Lesson number (e.g., 02)
        xx  - Exercise name (e.g., cmd)
        NNN - Student name (e.g., Cohen_Moshe)
    '''
    exe_id, exe_name, lesson_num = exercise
    stu_id, stu_name = student
    url = EXERCISE_FILE_URL
    url += exe_id + '?student=' + stu_id + '&to_garde=true'
    response = session.get(url)

    soup = bs(response.text, 'html.parser')

    for a in soup.find_all('a'):
        if a.get('href') is None:
            continue

        if '.docx' in a.get('href'):
            file_url = CYBER_ROOT_URL + a.get('href')
            response = session.get(file_url, allow_redirects=True)
            file_name = lesson_num + '_' + stu_name + '.docx'
            print(f'Downloading {file_name}')
            open(out_dir + file_name, 'wb').write(response.content)
            return True
    return False


def get_exercises(session, course):
    '''
    Returns a list of all exercises, for a given course class

    Eache element of the list is a tuple containing:
        Exercise Id            (e.g., 1234567)
        Exercise Name          (e.g., cmd)
        Exercise Lesson Number (e.g., 01)
    '''
    exercises = []

    url = TO_GRADE_EXERCISE_URL + course + '?stay=true'
    response = session.get(url)
    soup = bs(response.text, 'html.parser')
    all_td = soup.find_all('td')
    for i, td in enumerate(all_td):
        try:
            if td.span.text.find('תרגיל') == -1:
                continue
        except BaseException:
            continue

        lesson_num = td.span.a.text[0:2]
        exe_id = td.a.get('href').split('/show/')[1]
        exe_name = td.a.text.replace(' ', '_')
        exercises.append((exe_id, exe_name, lesson_num))

    return exercises


def get_students(session, exe_id):
    '''
    Returns a list of students, waiting to be checked on a given exercise

    Each element of the list is a tuple containing:
        Student Id   (e.g., 1234567)
        Student Name (e.g., Cohen, Moshe)
    '''
    students = []

    url = STUDENT_EXERCISE_URL + exe_id
    response = session.get(url)
    soup = bs(response.text, 'html.parser')
    for a in soup.find_all('a'):
        if a.get('href') is None:
            continue
        if '/user/show/' in a.get('href'):
            student_id = a.get('href').split('/user/show/')[1]
            student_name = a.text.replace(', ', '_')
            students.append((student_id, student_name))

    return students


def get_course_name(session, course):
    '''
    Returns the name of the course
    '''
    course_name = ''
    teacher_name = ''
    url = COURSE_STUDENTS_URL + course
    response = session.get(url)
    soup = bs(response.text, 'html.parser')
    for h1 in soup.find_all('h1'):
        course_name = h1.text.strip()
        break

    for span in soup.find_all('span'):
        if span.get('class') is None:
            continue

        if 'name' in span.get('class'):
            teacher_name = span.text
            break

    return course_name, teacher_name


def convert_word(exe, student, path):
    exe_id, exe_name, lesson_num = exe
    stu_id, stu_name = student
    file_name = lesson_num + '_' + exe_name + '_' + stu_name + '.docx'
    document = Document(path + file_name)
    n = Document()
    table = document.tables
    for i, t in enumerate(table):
        if len(t.rows[0].cells) == 1:
            paragraph = n.add_paragraph(
                "".join(
                    [str((i + 1)), ".\n", t.rows[0].cells[0].text,
                     "\n-------------"])
            )
        else:
            paragraph = n.add_paragraph(
                "".join([str((i + 1)), ".\n"])
            )
            template = t
            tbl = template._tbl
            new_tbl = copy.deepcopy(tbl)
            paragraph = n.add_paragraph()
            paragraph._p.addnext(new_tbl)
            paragraph = n.add_paragraph(
                "".join(["\n", "\n-------------"])
            )

    images(path + file_name, n)
    n.save(path + "Conv_" + file_name)
    print(f'Converting Conv_{file_name}')


def images(filename, document):
    z = zipfile.ZipFile(filename)
    all_files = z.namelist()
    images = [f for f in all_files if f.startswith('word/media/')]

    for i in images:
        image1 = z.open(i).read()
        f = open(i[-10:], 'wb')
        f.write(image1)
        f.close()
        document.add_picture(i[-10:], width=Inches(5))


def get_params(argv):
    '''
    Get parameters, either from JSON config file, or from execution
    arguments. Execution arguments override config file definitions.
    '''
    error_found = False
    course = ""
    lesson = ""
    exercise = ""
    out_dir = ""
    user_name = ""
    password = ""
    need_convert = False
    try:
        opts, _ = getopt.getopt(argv, "hc:l:e:o:u:p:n",
                                ["help",
                                 "course=",
                                 "lesson=",
                                 "exercise=",
                                 "output=",
                                 "user=",
                                 "password=",
                                 "convert"])
    except BaseException:
        pass

    else:
        for opt, arg in opts:
            if opt in ("-h", "--help"):
                print_usage()
                sys.exit(2)
            elif opt in ("-l", "--lesson"):
                lesson = arg
            elif opt in ("-e", "--exercise"):
                exercise = arg
            elif opt in ("-c", "--course"):
                course = arg
            elif opt in ("-o", "--output"):
                out_dir = arg
            elif opt in ("-u", "--user"):
                user_name = arg
            elif opt in ("-p", "--password"):
                password = arg
            elif opt in ("-n", "--convert"):
                need_convert = True

    if user_name == "" or password == "":
        error_found = True
        print("ERROR: Neo user name or password missing")

    if course == "":
        error_found = True
        print("ERROR: Neo course id missing")

    if (out_dir != "") and not os.path.isdir(out_dir):
        error_found = True
        print(f"ERROR: Output dir {out_dir} not found")

    if error_found:
        sys.exit(2)

    return (user_name, password, course, lesson,
            exercise, out_dir, need_convert)


def print_usage():
    """
    Prints usage message
    """
    print()
    print("NeoLoader dowloads all sudent submission files (.docx), "
          "for a MAGSHIMIM course lesson, from the Neo system")
    print()
    print("Developed by: Raz Nitzan (razn@cyber.org.il), November 2020")
    print()
    print("USAGE: NeoLoader.exe [Arguments]")
    print()
    print("Arguments:")
    print("    -h, --help               Shows this message")
    print("    -u  --user=USER          Neo user name")
    print("    -p  --password=PASSWORD  Neo password")
    print("    -c, --course=COURSE      Neo course number")
    print("    -l, --lesson=LESSON      Lesson number to download")
    print("    -e, --exercise=EXERCISE  Exercise id to download")
    print("    -o, --output=DIR         Output directory (optional)")
    print("    -n, --convert            Keep only answers in doc file")
    print()
    print("Examples:")
    print("    Load submissions for week 3 of course 2161735:")
    print("    > NeoLoader.exe -u moshe -p sod123 -c 2161735 -l 03")
    print()
    print("    Load submissions for exercise 17411265 of course 2161735:")
    print("    > NeoLoader.exe -u moshe -p sod123 -c 2161735 -e 17411265")


def main(argv):
    '''
    Downloads all student submissions for a given lesson of a course
    '''
    py_file = __file__
    exe_file = py_file.replace(".py", ".exe")
    try:
        version_date = time.ctime(os.path.getmtime(py_file))
    except BaseException:
        version_date = time.ctime(os.path.getmtime(exe_file))
    finally:
        print(f"NeoLoader Version {VERSION}, {version_date}\n")

    user_name, password, course, lesson, exercise, out_dir, need_convert = \
        get_params(argv)

    with requests.Session() as session:
        # Obtain session key
        url = CYBER_ROOT_URL + '/log_in/submit'
        url += '?userid=' + user_name
        url += '&password=' + password
        response = session.get(url)
        set_cookies = response.headers['Set-Cookie'].split(';')
        for c in set_cookies:
            if c.startswith('secure_lmssessionkey2'):
                session.cookies['secure_lmssessionkey2'] = c.split('=')[1]
                break
        else:
            sys.exit(2)

        course_name, teacher_name = get_course_name(session, course)
        exercises = get_exercises(session, course)

        for exe in exercises:
            exe_id, _, exe_lesson = exe
            if (lesson == "") or \
                    (exe_lesson == lesson) or (exe_id == exercise):
                students = get_students(session, exe_id)
                for stu in students:
                    is_done = download_submission_file(
                        session, exe, stu, out_dir)
                    if is_done and need_convert:
                        convert_word(exe, stu, out_dir)
        else:
            if lesson != "":
                print(f"ERROR: Lesson '{lesson}' not found, " +
                      f"in course {course_name}")
                sys.exit(2)


if __name__ == '__main__':
    main(sys.argv[1:])
