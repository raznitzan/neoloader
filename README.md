# NeoLoader

NeoLoader dowloads all sudent submission files (.docx), for a MAGSHIMIM course lesson, from the Neo system

Developed by: Raz Nitzan (razn@cyber.org.il), November 2020

USAGE: NeoLoader.exe [Arguments]

Arguments:

    -h, --help               Shows this message
    -u  --user=USER          Neo user name
    -p  --password=PASSWORD  Neo password
    -c, --course=COURSE      Neo course number
    -l, --lesson=LESSON      Lesson number to download
    -e, --exercise=EXERCISE  Exercise id to download
    -o, --output=DIR         Output directory (optional)

Examples:

    Load submissions for week 3 of course 2161735:
    > NeoLoader.exe -u moshe -p sod123 -c 2161735 -l 03

    Load submissions for exercise 17411265 of course 2161735:
    > NeoLoader.exe -u moshe -p sod123 -c 2161735 -e 17411265